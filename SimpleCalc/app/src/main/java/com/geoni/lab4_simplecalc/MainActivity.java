package com.geoni.lab4_simplecalc;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //definim editText-urile
        TextView rezultat = findViewById(R.id.textview);
        EditText primulNumar = findViewById(R.id.editTextNumber);
        EditText aldoileaNumar = findViewById(R.id.editTextNumber2);
        // definire butoane
        Button buttonAdunare = findViewById(R.id.button);
        Button buttonScadere = findViewById(R.id.button4);
        Button buttonInmultire = findViewById(R.id.button5);
        Button buttonImpartire = findViewById(R.id.button2);
        int primulNumarluat = Integer.parseInt(primulNumar.getText().toString());
    int aldoileaNumarluat = Integer.parseInt(aldoileaNumar.getText().toString());
        // actiune buton
       buttonAdunare.setOnClickListener(view -> rezultat.setText(String.valueOf(primulNumarluat+aldoileaNumarluat)));
        buttonImpartire.setOnClickListener(view-> rezultat.setText(String.valueOf(primulNumarluat/aldoileaNumarluat)));
        buttonInmultire.setOnClickListener(view -> rezultat.setText(String.valueOf(primulNumarluat*aldoileaNumarluat)));
        buttonScadere.setOnClickListener(view -> rezultat.setText(String.valueOf(primulNumarluat-aldoileaNumarluat)));
    }

}