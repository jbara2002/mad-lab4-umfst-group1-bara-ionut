package com.geoni.bookapp2;


import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm = getSupportFragmentManager();
        Button homeBtn = findViewById(R.id.chapter1Btn);
        // code for switching to chapter 1
        homeBtn.setOnClickListener(v -> fm.beginTransaction()
                .replace(R.id.fragmentContainerView2, chapterOneFragment.class,null)
                .setReorderingAllowed(true)
                .addToBackStack("name")
                .commit()) ;

        /// code for switching to chapter 2
        Button btnPreviousChapter = findViewById(R.id.chapter2Btn);
        btnPreviousChapter.setOnClickListener(v -> fm.beginTransaction()
                .replace(R.id.fragmentContainerView2, chapterTwoFragment.class,null)
                .setReorderingAllowed(true)
                .addToBackStack("name")
                .commit()) ;

        /// code for switching to chapter 3
        Button btnNextChapter = findViewById(R.id.chapter3Btn);
        btnNextChapter.setOnClickListener(v -> fm.beginTransaction()
                .replace(R.id.fragmentContainerView2, chapterThreeFragment.class,null)
                .setReorderingAllowed(true)
                .addToBackStack("name")
                .commit()) ;
        /// code for switching to chapter 4
        Button btn4 = findViewById(R.id.chapter4Btn);
        btn4.setOnClickListener(v -> fm.beginTransaction()
                .replace(R.id.fragmentContainerView2, chapterFourFragment.class,null)
                .setReorderingAllowed(true)
                .addToBackStack("name")
                .commit()) ;
    }
}
